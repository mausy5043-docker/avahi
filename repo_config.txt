
# Don't change the settings below unless you know what you are doing!
service=avahi
host="$(hostname)"
container="${service}_on_${host}"
image="mausy5043/${container}:latest"

options+=("--name ${container}")
options+=("--restart unless-stopped")
options+=("-d")
options+=("-h ${container}")
options+=("-p 5353:5353")
options+=("-v ${HOME}/.config/docker/avahi-services:/etc/avahi/services")
