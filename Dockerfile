FROM alpine:latest
LABEL maintainer="Mausy5043"

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            bash \
            avahi \
            tzdata \
 && sed -i 's/#enable-dbus=yes/enable-dbus=no/g' /etc/avahi/avahi-daemon.conf \
 && rm -rf /var/cache/apk/*

ENV TZ=Europe/Amsterdam

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
 && echo $TZ > /etc/timezone

VOLUME /etc/avahi/services

EXPOSE 5353/udp

ENTRYPOINT ["avahi-daemon"]
