#!/bin/bash

# Find out where we're running from
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! "$(pwd)" == "${script_dir}" ]; then
  echo "Not allowed. First execute:"
  echo "cd ${script_dir}"
  echo " Then try again."
  exit 1
fi

# shellcheck disable=SC1090
source "${script_dir}/repo_config.txt"

# build a local image
# shellcheck disable=SC2154
docker build --rm -t "${image}" . || exit 1

# copy the configuration file to a user-managed location.
if [ ! -d "${HOME}/.config/docker/avahi-services" ]; then
  mkdir -p "${HOME}/.config/docker/avahi-services"
fi
cp -R "${script_dir}/container"/* "${HOME}/.config/docker/avahi-services/"
